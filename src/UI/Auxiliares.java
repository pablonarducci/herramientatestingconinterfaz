/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package UI;

import java.util.Collections;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JList;

/**
 *
 * @author Sabina
 */
public class Auxiliares {
    public static <T> void cargarLista( List<T> nombres, JList lista ){
        DefaultListModel model = new DefaultListModel();
        for( T s : nombres ){
            model.addElement(s);
        }
        lista.setModel(model);
        lista.setSelectedIndex(0);
    }

    static String getFormatedCode(String codigo) {
        return "<html>" + codigo
                .replace("<", "&lt;")
                .replace(">", "&gt;")
                .replace(" ", "&nbsp;")
                .replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;")
                .replace("for ", "<b style=\"color:red\">for</b>&nbsp;")
                .replace("for(", "<b style=\"color:red\">for</b>(")
                .replace("if ", "<b style=\"color:red\">if</b>&nbsp;")
                .replace("if(", "<b style=\"color:red\">if</b>(")
                .replace("while", "<b style=\"color:red\">while</b>&nbsp;")
                .replace("while(", "<b style=\"color:red\">while</b>(")
                .replace("case ", "<b style=\"color:red\">case</b>&nbsp;") 
                .replace("\n", "<br/>")
                + "</html>";
    }
}
